Rails.application.routes.draw do
  get 'welcome/index'
  root 'welcome#index'

  resources :home, only: [:index]
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }

  resources :teams do
    resources :users
    resources :tasks do
      member do
        get 'unassign'
      end
    end
  end
end
