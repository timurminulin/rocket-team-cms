class CreateTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :username
      t.string :email

      t.timestamps
    end
  end
end
