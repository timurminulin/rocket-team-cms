class RenameColumsTeam < ActiveRecord::Migration[5.1]
  def change
    rename_column :teams, :name, :title
    rename_column :teams, :username, :slack_id
  end
end
