class AddTeamToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :team, :text
  end
end
