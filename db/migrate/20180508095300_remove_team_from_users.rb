class RemoveTeamFromUsers < ActiveRecord::Migration[5.1]
  def up
    remove_column :users, :team
    remove_column :teams, :email
  end

  def down
    add_column :users, :team, :text
    add_column :teams, :email, :text
  end
end
