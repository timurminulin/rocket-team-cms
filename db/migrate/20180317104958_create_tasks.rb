class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.text :title
      t.text :description
      t.text :stage
      t.text :level
      t.integer :score

      t.timestamps
    end
  end
end
