class AddTeamToTask < ActiveRecord::Migration[5.1]
  def change
    add_reference :tasks, :team, index: true
  end
end
