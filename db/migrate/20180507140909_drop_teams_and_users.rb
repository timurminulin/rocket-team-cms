class DropTeamsAndUsers < ActiveRecord::Migration[5.1]
  def up
    drop_table :teams_and_users
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
