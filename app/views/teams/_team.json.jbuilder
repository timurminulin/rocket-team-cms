json.extract! team, :id, :name, :username, :email, :created_at, :updated_at
json.url team_url(team, format: :json)
