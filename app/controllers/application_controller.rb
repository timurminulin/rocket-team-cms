class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  
  def user_required
    if !user_signed_in?
     redirect_to user_slack_omniauth_authorize_path
    end
   end

   def team_required
    redirect_to user_slack_omniauth_authorize_path unless current_user
    @team = current_user.teams.find_by(id: (params[:team_id] || params[:id]))
    redirect_to user_slack_omniauth_authorize_path unless @team
   end

  def set_team_id
    @team = Team.find(params[:team_id])
  end

  def set_teams
    @teams = current_user.teams
  end

end
