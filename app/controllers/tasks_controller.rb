class TasksController < ApplicationController
  before_action :user_required
  before_action :set_team_id
  before_action :set_teams
  before_action :set_task, only: [:show, :edit, :update, :destroy, :unassign]

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = @team.tasks
  end

  # GET /tasks/1
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  def create
    @task = @team.tasks.create(task_params)

    if @task.save
      redirect_to team_tasks_path(@team), notice: 'Task was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /tasks/1
  def update
    if @task.update(task_params)
      redirect_to [@team, @task], notice: 'Task was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /tasks/1
  def destroy
    @task.destroy
    redirect_to team_tasks_path(@team), notice: 'Task was successfully destroyed.'
  end
  
  def unassign
    @task.update(user: nil)
    redirect_to [@team, @task], notice: 'Task was successfully unassigned.'
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = @team.tasks.find(params[:id])
    end    
   

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:title, :description, :stage, :level, :score, :team_id, :user_id)
    end
end
