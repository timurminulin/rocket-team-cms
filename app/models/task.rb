class Task < ApplicationRecord
  validates :title, presence: true
  validates :stage, presence: true
  belongs_to :user
  belongs_to :team

  LEVELS = ['Новичек', 'Середнячек', 'Мастер'].freeze

  def self.levels
    LEVELS
  end
end
