class User < ApplicationRecord
    has_many :tasks
    has_and_belongs_to_many :teams
    def self.from_omniauth(auth)
      where(email: auth.info.email).first_or_create do |user|
        user.email = auth.info.email
        user.name = auth.info.name 
        user.user_id = auth.info.user_id  
        team = Team.find_or_create_by(slack_id: auth.info.team_id) 
        team.update(title: auth[:extra][:team_info][:team][:name])
        user.teams << team unless user.teams.where(slack_id: auth.info.team_id).any?
      end
    end
    devise :omniauthable, omniauth_providers: %i[slack]
end